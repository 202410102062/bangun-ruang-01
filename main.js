const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, data => {
            resolve(data);
        });
    });
}

function hitungKerucut(radius, tinggi) {
  
   
    const volume = (1 / 3) * Math.PI * radius * radius * tinggi;
    
    console.log(`Volume: ${volume}`);
    
}

async function luasPermukaanLimas() {
    console.log(`
    ===VOLUME LIMAS SEGI TIGA===
    `);
    const a = await question('    Luas alas: ');
    const b = await question('    Tinggi limas: ');

    console.log(`    Volume Limas = ${1/3 * parseInt(a) * parseInt(b)}`);
}

function VolumePrisma(alas, tinggiAlas, tinggiPrisma){
  return (alas * tinggiAlas/2) * tinggiPrisma
}

function tabung(jari2, tinggi){
    return jari2 * jari2 * 3.14 * tinggi;
}

console.log(tabung(10, 10));
console.log(VolumePrisma(10,20,3))
hitungKerucut(6, 10);